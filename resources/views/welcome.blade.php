@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Product Form</h3>
              </div>
              <div class="panel-body">
                {!! Form::open(['url' => 'store', 'autocomplete' => 'off']) !!}
                <div class="form-group">
                    <label for="product-name">Product Name</label>
                    {!! Form::text('product_name', null, ['class' => 'form-control', 'placeholder' => 'Enter product name', 'id' => 'product-name']) !!}
                </div>
                <div class="form-group">
                    <label for="quantity">Quantity in Stock</label>
                    {!! Form::text('quantity', null, ['class' => 'form-control', 'placeholder' => 'Enter quantity', 'id' => 'quantity']) !!}
                </div>
                <div class="form-group">
                    <label for="price">Price per Item</label>
                    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Enter price per item', 'id' => 'price']) !!}
                </div>
                <div class="message" style="display: none"></div>
                <input type="hidden" name="update" value="" id="update">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Clear</button>
                {!! Form::close() !!}
              </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="product-table">
                    <thead>
                        <th>Product name</th>
                        <th class="text-center">Quantity in stock</th>
                        <th class="text-center">Price per item</th>
                        <th class="text-center">Datetime submitted</th>
                        <th class="">Total value number</th>
                        <th class="text-center">Actions</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6" class="text-muted text-center">No products added!</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')

<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
</script>
<script>
    $('form').submit(function(e){
        e.preventDefault();
        var data = $(this).serializeArray();
        var error_message = "<div class='alert alert-danger'><ul>";
        var table_data = "";
        var total = 0;

        $.ajax({
            'url'   : '{{ url('store') }}',
            'data'  : data,
            'type'  : 'POST',
            'dataType': 'json',
            beforeSend: function()
            {   
                $('.message').html('').hide();
                $('button[type="submit"]').text('Submitting...').attr('disabled', true);
            },  
            success: function(response)
            {
                $('.message').html("<div class='alert alert-success'>" + response.success+ '</div>').slideDown().delay(3000).slideUp();
                $('form')[0].reset();
                $('#update').val('');
                filltable(response.products);
            },
            error: function(response){
                var errors = response.responseJSON;
                $.each(errors, function(key, value){
                    error_message += '<li>' + value + '</li>';
                });
                $('.message').html(error_message+'</ul>').slideDown();
            },
            complete: function()
            {
                $('button[type="submit"]').text('Submit').attr('disabled', false);
            }
        });
    });

    function filltable(data)
    {
        var table_data = "";
        var total = 0;
        if(data.length == 0)
        {
            table_data = '<tr>\
                            <td colspan="6" class="text-muted text-center">No products added!</td>\
                        </tr>';
        }
        else
        {
            $.each(data, function(key, value){
                table_data  +=  '<tr>\
                                    <td>' +value.product_name+ '</td>\
                                    <td class="text-center">' +value.quantity+ '</td>\
                                    <td class="text-center">' +value.price+ '</td>\
                                    <td class="text-center">' +value.created_at+ '</td>\
                                    <td class="">' + (value.quantity*value.price).toFixed(2) + '</td>\
                                    <td class="text-center"><a href="'+value.id+'" class="edit-this">Edit</a> &nbsp;&nbsp; <a href="delete/'+value.id+'" class="delete-this">Delete</a></td>\
                                </tr>';
                total += value.quantity*value.price;
            });

            table_data += '<tr><td colspan="4" class="text-right"><b>Total</b></td><td colspan="2">' + total.toFixed(2) + '</td><tr>';
        }

        $('#product-table tbody').html(table_data);
    }

    $(document).ready(function(){
        $.post("{{ url('fetch') }}", function(data, status){
            filltable(data);
        });
    });

    $(document).on('click', '.edit-this', function(e){
        e.preventDefault();
        $('#update').val($(this).attr('href'));
        $('input[name="product_name"]').val($(this).closest('tr').children('td')[0].innerText);
        $('input[name="quantity"]').val($(this).closest('tr').children('td')[1].innerText);
        $('input[name="price"]').val($(this).closest('tr').children('td')[2].innerText);
        $('html, body').animate({ scrollTop: 0 }, "slow");
    });

    $(document).on('click', '.delete-this', function(e){
        e.preventDefault();
        var send = $(this).attr('href');

        $.ajax({
            url: send,
            type: 'POST',
            dataType: 'json',
            success: function(response)
            {
                /*$('.message').html("<div class='alert alert-success'>" + response.success+ '</div>').slideDown().delay(3000).slideUp();*/
                filltable(response.products);
            }
        })
    });

    $('button[type="reset"]').click(function(){
        $('#update').val('');
    });

</script>

@stop