<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use App\Product;
use File;

class ProductController extends Controller
{
	/**
	 * Show Product form and list them
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function index()
	{
		return view('welcome');
	}

	/**
	 * Store product
	 */
    public function store(ProductRequest $request)
    {
    	$input = $request->all();
    	if($input['update'] == "")
    	{
    		$data = Product::create($input);
    	}
    	else
    	{
    		$product = Product::find($input['update']);
    		$product->update($input);
    	}
    	
    	$content = Product::latest()->get();
    	$success = File::put('data.json', $content->toJson());
    	if($success)
    		return response()->json(['success' => 'Product saved', 'products' => $content], 202);
    	else
    		return response()->json(['error' => 'Something went wrong!'], 422);
    }

    /**
     * Fetch all products
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function fetch()
    {
    	return Product::latest()->get();
    }

    public function delete($id)
    {
    	$product = Product::destroy($id);
    	$content = Product::latest()->get();
    	$success = File::put('data.json', $content->toJson());
    	if($success)
    		return response()->json(['success' => 'Product deleted', 'products' => $content], 202);
    	else
    		return response()->json(['error' => 'Something went wrong!'], 422);
    }
}
