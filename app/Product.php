<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ['product_name', 'quantity', 'price'];

	protected $appends = ['total_value'];

	public function getTotalValueAttribute()
    {
        return $this->attributes['quantity']*$this->attributes['price'];
    }
}
